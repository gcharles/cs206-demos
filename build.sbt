name := "cs206-demos"
version := "0.1.2"
scalaVersion := "3.2.0"
libraryDependencies ++= Seq(
  ("com.storm-enroute" %% "scalameter-core" % "0.21").cross(CrossVersion.for3Use2_13),
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
  "junit" % "junit" % "4.13" % Test,
  "com.github.sbt" % "junit-interface" % "0.13.3" % Test,
)
scalacOptions ++= Seq("-unchecked", "-deprecation")

Test / parallelExecution := false
Test / testOptions += Tests.Argument(TestFrameworks.JUnit)

enablePlugins(JmhPlugin, ScalafmtPlugin)
