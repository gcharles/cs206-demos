package midterm22

import instrumentation.Monitor

// Question 21

// See tests in midterm22.Part6Test.
// Run with `sbt testOnly midterm22.Part6Test`.

class TicketsManager(totalTickets: Int) extends Monitor:
  var remainingTickets = totalTickets

  // This method might be called concurrently
  def getTicket(): Boolean =
    if remainingTickets > 0 then
      this.synchronized {
        remainingTickets -= 1
      }
      true
    else false
