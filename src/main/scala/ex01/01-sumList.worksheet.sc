// This is a minimal worksheet example. If you open the folder cs206-demos in
// VSCode and then display this source, you should see all top-level statements
// evaluated in comments, as in:
//
//    import scala.annotation.tailrec
// 
//    val testList = List(1, 2, 3) // List[Int] = List(1, 2, 3)
// 
//    testList.head // Int = 1
//    testList.isEmpty // Boolean = false
//    testList.tail // List[Int] = List(2, 3)
//    testList.tail.head // Int = 2
//    testList ++ List(4, 6) // List[Int] = List(1, 2, 3, 4, 6)
//
//    ...

import scala.annotation.tailrec

val testList = List(1, 2, 3)

testList.head
testList.isEmpty
testList.tail
testList.tail.head
testList ++ List(4, 6)

def sumList(ls: List[Int]) =
  @tailrec
  def loop(ls: List[Int], acc: Int): Int =
    if ls.isEmpty then acc
    else loop(ls.tail, acc + ls.head)
  loop(ls, 0)

sumList(List())
sumList(List(1, 5))
sumList(List(6, 7, 8, 9))
