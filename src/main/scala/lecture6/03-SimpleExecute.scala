package lecture6

import scala.concurrent.ExecutionContext
def execute(body: => Unit) =
  ExecutionContext.global.execute(new Runnable:
    override def run() = body
  )

@main def SimpleExecute =
  execute { log("This task is run asynchronously.") }
  Thread.sleep(500)
