#!/usr/bin/env bash

run=2

run_benchmark() {
    name=$1
    jvm=$2
    json_file=benchmarks/results/$name-$jvm-$run.json
    stdout_file=benchmarks/results/$name-$jvm-$run.txt

    rm -rf "$json_file" "$stdout_file" .bloop .sbt .bsp .metals target
    sbt "clean; Jmh / run -wi 5 -w 2 -i 5 -r 2 -f 4 -rf JSON -rff $json_file ${name:3}" >$stdout_file 2>&1
}

run_benchmarks() {
    jvm=$1
    jvm_coursier_id=$2

    eval "$(coursier java --jvm "$jvm_coursier_id" --env)"

    for bench in "01-AppendBenchmark"; do
        run_benchmark $bench $jvm
    done
}

run_benchmarks graal "graalvm-java17:22.3.1"
run_benchmarks openjdk "adoptium:1.17.0.6"
